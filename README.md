# Create a Raspberry Pi Time lapse!
Use a Raspberry Pi and a USB webcam to create a time lapse.

![Example Image](https://gitlab.com/onii/raspberrypi-timelapse/raw/master/Example.jpeg)

Features include:

* Timestamp
* Raspberry Pi logo
* Darkness detection, remove dark images automatically
* Update a web directory for "live" view
* Adjust focus of your camera

Requires `imagemagick` and `streamer`. *Web server (`nginx` recommended) optional*.

To install:

    sudo apt install git imagemagick streamer
    git clone https://gitlab.com/onii/raspberrypi-timelapse.git

Cron example:

    */5 * * * * ~/raspberrypi-timelapse/Timelapse.sh

A couple examples for encoding the image sequence:

`mencoder mf://*.jpeg -mf w=1280:h=720:fps=12:type=jpg -ovc x264 -x264encopts preset=medium:tune=film:crf=26 -o "../2013-07-18.mp4"`

`ffmpeg -f image2 -i %*.jpeg -vf scale=1280:720 -r 12 -c:v libx264 -preset medium -tune film -crf 26 2013-07-18.mp4`
