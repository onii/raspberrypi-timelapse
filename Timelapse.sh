#!/bin/bash
#
# Raspberry Pi Timelapse
#
time=`date +%Y-%m-%d_%H-%M` # Filename
text="\        `date +%a`. `date +%l:%M%p` [`date +%x`] " # Caption
res="1280x720" # Resolution of webcam
dir="Pictures" # Storage location
#web="/var/www/html" # Web location

#uvcdynctrl --device=/dev/video0 --set='Focus' 35
# Camera focus .. 1 (infinity) - 60 or 70'ish (macro)

function timelapse {
    streamer -c /dev/video0 -s "${res}" -j 100 -q -b 16 -o "${dir}/${time}.jpeg"
    sleep 5
    mean=`convert ${dir}/${time}.jpeg -format "%[mean]" info:` # Brightness eval
    thresh=10 # Brightness threshold
    test=`convert xc: -format "%[fx:(100*$mean/quantumrange)<$thresh)?1:0]" info:`
    if [[ $test -eq 1 ]]; then # If too dark
        rm "${dir}/${time}.jpeg"
        if [[ $web ]]; then
	    echo "web!"
            cp "too-dark.jpeg" "${web}/latest.jpeg"
        fi
    else
        convert "${dir}/${time}.jpeg" -fill '#bc1142' -undercolor '#75a92880' \
            -gravity northwest -pointsize 34 \
            -font "Ubuntu-B.ttf" \
            -annotate +10+10 "$text" \
            -gravity northwest -draw "image Over 6,6 0,0 'Raspberry_Pi_Logo-64.png'" \
            "${dir}/${time}.jpeg"
        if [[ $web ]]; then
            sleep 5
            convert "${dir}/${time}.jpeg" -adaptive-resize 640x480 -quality 90% "${web}/latest.jpeg"
        fi
    fi
}

if [ -c /dev/video0 ]; then # If webcam is plugged in
    timelapse
fi
